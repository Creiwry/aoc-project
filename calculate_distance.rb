require_relative "lib/day11.rb"
require_relative "root.rb"

universe = Universe.new("#{ROOT_DIR}/data/day11_data.txt")
universe.expansion_multiplier = 1_000_000
result = universe.distance_between_all_galaxies

File.open("#{ROOT_DIR}/results.txt", "a") { |f| f << "day 11 result multiplier 1 000 000: #{result} \n" }
