# frozen_string_literal: true

RSpec.describe AocProject do
  it "has a version number" do
    expect(AocProject::VERSION).not_to be nil
  end
end
