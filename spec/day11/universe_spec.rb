# frozen_string_literal: true

require_relative "../../lib/day11/universe.rb"
require_relative "../../lib/day11/galaxy.rb"
require_relative "../../root.rb"

RSpec.describe Universe do
  let(:universe) { Universe.new("#{ROOT_DIR}/spec/fixtures/universe1.txt") }
 
  let(:expected_universe_array) {
    [
      "...#......",
      ".......#..", 
      "#.........",
      "..........",
      "......#...",
      ".#........",
      ".........#",
      "..........",
      ".......#..",
      "#...#....."
    ]
  }

  let(:expected_universe_hash) {
    {
      0 => [".", ".", ".", "#", ".", ".", ".", ".", ".", "."],
      1 => [".", ".", ".", ".", ".", ".", ".", "#", ".", "."],
      2 => ["#", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      3 => [".", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      4 => [".", ".", ".", ".", ".", ".", "#", ".", ".", "."],
      5 => [".", "#", ".", ".", ".", ".", ".", ".", ".", "."],
      6 => [".", ".", ".", ".", ".", ".", ".", ".", ".", "#"],
      7 => [".", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      8 => [".", ".", ".", ".", ".", ".", ".", "#", ".", "."],
      9 => ["#", ".", ".", ".", "#", ".", ".", ".", ".", "."]
    }
  }

  let(:expected_galaxy_coordinates) {
    [
      [0,3],
      [1,7],
      [2,0],
      [4,6],
      [5,1],
      [6,9],
      [8,7],
      [9,0],
      [9,4],
    ]
  }

  let(:expected_unique_galaxy_pairs) {
    [
      [[0, 3], [1, 7]],
      [[0, 3], [2, 0]],
      [[0, 3], [4, 6]],
      [[0, 3], [5, 1]],
      [[0, 3], [6, 9]],
      [[0, 3], [8, 7]],
      [[0, 3], [9, 0]],
      [[0, 3], [9, 4]],
      [[1, 7], [2, 0]],
      [[1, 7], [4, 6]],
      [[1, 7], [5, 1]],
      [[1, 7], [6, 9]],
      [[1, 7], [8, 7]],
      [[1, 7], [9, 0]],
      [[1, 7], [9, 4]],
      [[2, 0], [4, 6]],
      [[2, 0], [5, 1]],
      [[2, 0], [6, 9]],
      [[2, 0], [8, 7]],
      [[2, 0], [9, 0]],
      [[2, 0], [9, 4]],
      [[4, 6], [5, 1]],
      [[4, 6], [6, 9]],
      [[4, 6], [8, 7]],
      [[4, 6], [9, 0]],
      [[4, 6], [9, 4]],
      [[5, 1], [6, 9]],
      [[5, 1], [8, 7]],
      [[5, 1], [9, 0]],
      [[5, 1], [9, 4]],
      [[6, 9], [8, 7]],
      [[6, 9], [9, 0]],
      [[6, 9], [9, 4]],
      [[8, 7], [9, 0]],
      [[8, 7], [9, 4]],
      [[9, 0], [9, 4]]
    ]
  }

  let(:expected_empty_rows) { [3,7] }
  let(:expected_empty_columns) { [2, 5, 8] }
  
  it "initializes with file path" do
    expect(universe.universe_array).to eq(expected_universe_array)
  end

  describe "universe method" do
    it "returns a hash mapping the universe" do
      expect(universe.universe).to eq(expected_universe_hash)
    end
  end

  describe "galaxies attribute" do
    it "returns all galaxies in the universe with correct coordinates" do
      galaxy_coordinates = universe.galaxies.map(&:coordinates)
      expect(galaxy_coordinates).to eq(expected_galaxy_coordinates)
    end
  end

  describe "empty_columns method" do
    it "returns an array of the indexes of empty columns" do
      expect(universe.empty_columns).to eq(expected_empty_columns)
    end
  end

  describe "empty_rows method" do
    it "returns an array of the keys of empty rows" do
      expect(universe.empty_rows).to eq(expected_empty_rows)
    end
  end

  describe "unique_galxy_pairs method" do
    it "returns a hash of unique galaxy pair coordinates" do
      unique_galaxy_pair_coordinates = universe.unique_galaxy_pairs.map { |pair| pair.map(&:coordinates) }
      expect(unique_galaxy_pair_coordinates).to eq(expected_unique_galaxy_pairs)
    end
  end

  describe "distance_with_shift method" do
    context "when no expansion index is provided" do
      it "returns an integer of the minimal distance between two galaxies with expansion multiplier of 2 taken into account" do
        expect(universe.distance_with_shift(Galaxy.new(5, 1), Galaxy.new(9, 4))).to eq(9)
        expect(universe.distance_with_shift(Galaxy.new(0,3), Galaxy.new(8,7))).to eq(15)
        expect(universe.distance_with_shift(Galaxy.new(0,3), Galaxy.new(5,1))).to eq(9)
        expect(universe.distance_with_shift(Galaxy.new(2,0), Galaxy.new(6,9))).to eq(17)
        expect(universe.distance_with_shift(Galaxy.new(9,0), Galaxy.new(9,4))).to eq(5)
        expect(universe.distance_with_shift(Galaxy.new(1,7), Galaxy.new(9,0))).to eq(19)
      end
    end

    context "when expansion index is provided" do
      before do
        universe.expansion_multiplier = 10
      end

      it "returns an integer of the minimal distance between two galaxies with set expansion multiplier taken into account" do
        expect(universe.distance_with_shift(Galaxy.new(0,3), Galaxy.new(8,7))).to eq(39)
        expect(universe.distance_with_shift(Galaxy.new(0,3), Galaxy.new(5,1))).to eq(25)
        expect(universe.distance_with_shift(Galaxy.new(2,0), Galaxy.new(6,9))).to eq(49)
        expect(universe.distance_with_shift(Galaxy.new(9,0), Galaxy.new(9,4))).to eq(13)
        expect(universe.distance_with_shift(Galaxy.new(1,7), Galaxy.new(9,0))).to eq(51)
      end
    end
  end

  describe "distance_between_all_galaxies method" do
    context "when no expansion index is provided" do
      it "returns an integer of the total distance between all the galaxies in the universe with assumed expansion multiplier of 2" do
        expect(universe.distance_between_all_galaxies).to eq(374)
      end
    end

    context "when expansion index is provided" do

      it "returns an integer of the total distance between all the galaxies in the universe with set expansion multiplier" do
        universe.expansion_multiplier = 10
        expect(universe.distance_between_all_galaxies).to eq(1030)
        universe.expansion_multiplier = 100
        expect(universe.distance_between_all_galaxies).to eq(8410)
      end
    end
  end
end
