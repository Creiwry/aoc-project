# frozen_string_literal: true

require_relative "../../lib/day11/galaxy.rb"

RSpec.describe Galaxy do
  it "gets initialized with coordinates" do
    galaxy = Galaxy.new(0, 3)
    expect(galaxy.row).to eq(0)
    expect(galaxy.column).to eq(3)
  end
end
