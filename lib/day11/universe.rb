# frozen_string_literal: true
require_relative "./galaxy.rb"

class Universe
  attr_accessor :expansion_multiplier, :galaxies

  def initialize(file_name, expansion_multiplier = 2)
    @expansion_multiplier = expansion_multiplier
    @file_name = file_name
    @galaxies = []
    populate_galaxies
  end

  def universe_array
    array = []
    File.readlines(@file_name, chomp: true).each { |line| array << line }
    array
  end

  def universe
    universe_hash = {}
    universe_array.each_with_index do |line, index|
      universe_hash[index] = line.split("")
    end
    universe_hash
  end

  def populate_galaxies
    universe.each_pair do |key, value|
      value.each_with_index { |point, index| @galaxies << Galaxy.new(key, index) if point == "#" }
    end
  end

  def empty_rows
    galaxy_rows = galaxies.map(&:row)
    universe_rows = (0..universe.keys.last).to_a
    galaxy_rows - universe_rows | universe_rows - galaxy_rows
  end

  def empty_columns
    galaxy_columns = galaxies.map(&:column)
    universe_columns = (0..(universe[0].length - 1)).to_a
    galaxy_columns - universe_columns | universe_columns - galaxy_columns
  end

  def unique_galaxy_pairs
    galaxies.combination(2).to_a
  end

  def distance_with_shift(first_galaxy, second_galaxy)
    start_column = [first_galaxy.column, second_galaxy.column].min 
    end_column = [first_galaxy.column, second_galaxy.column].max
    expanded_columns = (start_column..end_column).to_a & empty_columns

    start_row = [first_galaxy.row, second_galaxy.row].min 
    end_row = [first_galaxy.row, second_galaxy.row].max
    expanded_rows = (start_row..end_row).to_a & empty_rows
    total_expanded_spaces = (expanded_rows.length + expanded_columns.length)

    shift_distance = (total_expanded_spaces * expansion_multiplier) - total_expanded_spaces

    (first_galaxy.row - second_galaxy.row).abs + (first_galaxy.column - second_galaxy.column).abs + shift_distance
  end

  def distance_between_all_galaxies
    unique_galaxy_pairs.sum do |galaxy_pair|
      distance_with_shift(*galaxy_pair)
    end 
  end
end
