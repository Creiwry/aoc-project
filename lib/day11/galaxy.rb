class Galaxy
  attr_accessor :row, :column, :coordinates

  def initialize(row, column)
    @row = row
    @column = column
    @coordinates = [row, column]
  end
end
