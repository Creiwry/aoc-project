# frozen_string_literal: true

require_relative "aoc_project/version"

module AocProject
  class Error < StandardError; end
  # Your code goes here...
end
