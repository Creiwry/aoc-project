# AOC project

### What is this?

These are my solutions to some Advent of Code 2023 problems. The results of each day's roblems can be found in results.txt

## Day 11

### How to run calculations for day 11?

you need ruby to run this code. The ruby verions of this project is ruby 3.2.2. If you have ruby installed, run:

```bash
bundle install
```
this installs all necessary dependencies.

Currently, the results have already been output into results.txt. If you want to re-run the calculations, comment out the update of the expansion_multiplier attribute on he universe object for the first result and run:

```bash
ruby calculate_distance.rb
```
for the second result, uncomment the expansion_multiplier attribute update (set to 1_000_000) and re-run the command above

### Where is the code?
all code for day 11 is stored in lib/day11 directory. There are two files, each containing a class (Universe, Galaxy).
Both classes are tested and the tests provide documetation. You can find the tests in the spec directory. The structure of the spec directory mirrors the lib/ directory. 

You can run all tests like so:
```bash
rspec
```

### Is there more information?
my thought process while writig this code is outlined in the pseudo/day11_pseudo.pdf file. It should clarify the logic of my decisions, but the tests should provide enough documentation for the functionality of the classes. Likewise, commit messages should provide additional doccumentation.


